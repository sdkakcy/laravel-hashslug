<?php

/*

Laravel HashSlug: Package providing a trait to use Hashids on a model
Copyright (C) 2017-2024  Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace Balping\HashSlug\Tests;


use Illuminate\Database\Eloquent\Model;
use Balping\HashSlug\HasHashSlug;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Controller;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use PHPUnit\Framework\Attributes\Test;

class HashSlugTest extends \Orchestra\Testbench\TestCase
{
	use InteractsWithExceptionHandling;

	public function setUp() : void {
		parent::setUp();
		$this->configureDatabase();
	}

	protected function configureDatabase(){
		$db = new DB;
		$db->addConnection([
			'driver'    => 'sqlite',
			'database'  => ':memory:',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		]);
		$db->bootEloquent();
		$db->setAsGlobal();
		DB::schema()->create('posts', function ($table) {
			$table->increments('id');
			$table->string('title');
			$table->timestamps();
		});

		DB::schema()->create('comments', function ($table) {
			$table->increments('id');
			$table->string('body');
			$table->timestamps();
		});
	}

	#[Test]
	public function model_has_a_unique_slug(){
		$post1 = Post::forceCreate(["title" => "title1"]);

		$slug1 = $post1->slug();

		$this->assertEquals(5, strlen($slug1));

		$post2 = Post::forceCreate(["title" => "title2"]);

		$slug2 = $post2->slug();

		$this->assertFalse($slug1 == $slug2);
	}

	#[Test]
	public function model_can_be_found_by_slug(){
		$post = Post::forceCreate(["title" => "title1"]);

		$slug = $post->slug();

		$foundPost = Post::findBySlugOrFail($slug);

		$this->assertEquals($post->id, $foundPost->id);
	}

	#[Test]
	public function trait_can_be_used_on_basemodel(){
		$post = PostExtendingBase::forceCreate(["title" => "title1"]);

		$slug = $post->slug();

		$foundPost = PostExtendingBase::findBySlugOrFail($slug);

		$this->assertEquals($post->id, $foundPost->id);
	}

	#[Test]
	public function find_by_invalid_slug_returns_null(){
		$post = Post::forceCreate(["title" => "title1"]);

		$slug = 'XX' . $post->slug();

		$foundPost = Post::findBySlug($slug);

		$this->assertNull($foundPost);
	}

	#[Test]
	public function slugs_are_different_for_same_id_but_different_model(){
		$post = Post::forceCreate(["title" => "title1"]);

		$comment = Comment::forceCreate(["body" => "comment1"]);

		$this->assertEquals($post->id, $comment->id);

		$this->assertNotEquals($post->slug(), $comment->slug());
	}

	#[Test]
	public function slug_padding_can_be_set(){
		$post = PostLongSlug::forceCreate(["title" => "title1"]);

		$this->assertEquals(10, strlen($post->slug()));
	}

	#[Test]
	public function prefix_can_be_customised(){
		$post = PostCustomPrefix::forceCreate(["title" => "title1"]);

		$this->assertStringStartsWith('post-', $post->slug());
	}

	#[Test]
	public function model_can_be_found_by_prefixed_slug(){
		$post = PostCustomPrefix::forceCreate(["title" => "title1"]);

		$slug = $post->slug();

		$foundPost = PostCustomPrefix::findBySlugOrFail($slug);

		$this->assertEquals($post->id, $foundPost->id);
	}

	#[Test]
	public function find_by_invalid__prefixed_slug_returns_null(){
		$post = PostCustomPrefix::forceCreate(["title" => "title1"]);

		$slug = 'XX' . $post->slug();

		$foundPost = PostCustomPrefix::findBySlug($slug);

		$this->assertNull($foundPost);
	}

	#[Test]
	public function model_salt_can_be_customised(){
		$post = PostCustomSalt::forceCreate(["title" => "title1"]);
		$comment = CommentCustomSalt::forceCreate(["body" => "comment1"]);

		$this->assertEquals($post->slug(), $comment->slug());
	}

	#[Test]
	public function alphabet_can_be_customised(){
		$post = PostCustomAlphabet::forceCreate(["title" => "title1"]);

		$this->assertMatchesRegularExpression('/^[A-Z]{50}$/', $post->slug());
	}

	#[Test]
	public function urls_are_generated_using_slug(){
		Route::resource('/posts-nobind', '\Balping\HashSlug\Tests\PostControllerNoBind', [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		]);

		$post = Post::forceCreate(["title" => "title1"]);

		$this->assertEquals(
			'http://localhost/posts-nobind/' . $post->slug(),
			action('\Balping\HashSlug\Tests\PostControllerNoBind@show', $post)
		);

		$response = $this->get('/posts-nobind/' . $post->slug());

		$this->assertEquals($post->slug(), $response->getContent());
	}

	#[Test]
	public function implicit_route_model_binding(){
		Route::resource('/posts', '\Balping\HashSlug\Tests\PostController', [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		]);

		$post = Post::forceCreate(["title" => "title1"]);

		$this->app['config']->set('app.debug', true);

		$response = $this->get('/posts/' . $post->slug());

		$this->assertEquals($post->slug(), $response->getContent());
	}

	#[Test]
	public function explicit_route_model_binding(){
		Route::model('article', Post::class);

		Route::resource('/articles', '\Balping\HashSlug\Tests\PostController', [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		]);

		$post = Post::forceCreate(["title" => "title1"]);

		$this->app['config']->set('app.debug', true);

		$response = $this->get('/articles/' . $post->slug());

		$this->assertEquals($post->slug(), $response->getContent());
	}

	#[Test]
	public function urls_are_generated_using_id_if_requested(){
		Route::resource('/posts-nobind', '\Balping\HashSlug\Tests\PostControllerNoBindId', [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		]);

		$post = Post::forceCreate(["title" => "title1"]);

		$this->assertEquals(
			'http://localhost/posts-nobind/' . $post->id,
			action('\Balping\HashSlug\Tests\PostControllerNoBindId@show', $post->id)
		);

		$response = $this->get('/posts-nobind/' . $post->id);

		$this->assertEquals($post->slug(), $response->getContent());
	}


	#[Test]
	public function urls_are_generated_using_id_even_with_implicit_binding_if_requested(){
		Route::resource('/posts', PostController::class, [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		])->parameters([
			"posts"	=>	"post:id"
		]);

		$post = Post::forceCreate(["title" => "title1"]);

		$this->assertEquals(
			'http://localhost/posts/' . $post->id,
			action('\Balping\HashSlug\Tests\PostController@show', $post)
		);

		$response = $this->get('/posts/' . $post->id);

		$this->assertEquals($post->slug(), $response->getContent());
	}

	#[Test]
	public function implicit_route_model_binding_without_hashslug(){
		Route::resource('/posts', PostController::class, [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		])->parameters([
			"posts"	=>	"post:id"
		]);

		$post = Post::forceCreate(["title" => "title1"]);

		$this->app['config']->set('app.debug', true);

		$response = $this->get('/posts/' . $post->id);

		$this->assertEquals($post->slug(), $response->getContent());

		Route::get('/posts2/{post:id}', [PostController::class, 'show'])->middleware(\Illuminate\Routing\Middleware\SubstituteBindings::class);

		$response = $this->get('/posts2/' . $post->id);

		$this->assertEquals($post->slug(), $response->getContent());
	}

	#[Test]
	public function explicit_route_model_binding_without_hashslug(){
		Route::model('article', Post::class);

		Route::resource('/articles', '\Balping\HashSlug\Tests\PostController', [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		])->parameters([
			"articles"	=>	"post:id"
		]);

		$post = Post::forceCreate(["title" => "title1"]);

		$this->app['config']->set('app.debug', true);

		$response = $this->get('/articles/' . $post->id);

		$this->assertEquals($post->slug(), $response->getContent());
	}

	#[Test]
	public function implicit_route_model_binding_without_hashslug_with_function_override(){
		Route::resource('/posts', PostControllerCustomRouteBindingOverridden::class, [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		]);

		$post = PostCustomRouteBindingOverridden::forceCreate(["title" => "title1"]);

		$this->app['config']->set('app.debug', true);

		$response = $this->get('/posts/' . $post->id);

		$this->assertEquals($post->slug(), $response->getContent());
	}

	#[Test]
	public function explicit_route_model_binding_without_hashslug_with_function_override(){
		Route::model('article', PostCustomRouteBindingOverridden::class);

		Route::resource('/articles', '\Balping\HashSlug\Tests\PostControllerCustomRouteBindingOverridden', [
			"middleware"	=> \Illuminate\Routing\Middleware\SubstituteBindings::class
		]);

		$post = PostCustomRouteBindingOverridden::forceCreate(["title" => "title1"]);

		$this->app['config']->set('app.debug', true);

		$response = $this->get('/articles/' . $post->id);

		$this->assertEquals($post->slug(), $response->getContent());
	}

}

class Post extends Model {
	use HasHashSlug;
}

class PostLongSlug extends Model {
	use HasHashSlug;

	protected static $minSlugLength = 10;

	protected $table = "posts";
}

class PostCustomSalt extends Model {
	use HasHashSlug;

	protected static $modelSalt = "customsalt";

	protected $table = "posts";
}

class PostCustomAlphabet extends Model {
	use HasHashSlug;

	protected static $minSlugLength = 50; //reduce chance
	protected static $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

	protected $table = "posts";
}

class PostCustomPrefix extends Model {
	use HasHashSlug;

	protected static $hashSlugPrefix = 'post-';

	protected $table = "posts";
}

class CommentCustomSalt extends Model {
	use HasHashSlug;

	protected static $modelSalt = "customsalt";

	protected $table = "comments";
}

class PostCustomRouteBindingOverridden extends Model {
	protected $table = "posts";

	use HasHashSlug;

	public function getRouteKeyName(){
		return parent::getRouteKeyName();
	}

	public function getRouteKey(){
		return parent::getRouteKey();
	}
}


class Comment extends Model {
	use HasHashSlug;
}

class PostController extends Controller {
	public function show(Post $post){
		return $post->slug();
	}
}

class PostControllerNoBind extends Controller {
	public function show($slug){
		$post = Post::findBySlugOrFail($slug);
		return $post->slug();
	}
}

class PostControllerNoBindId extends Controller {
	public function show($id){
		$post = Post::findOrFail($id);
		return $post->slug();
	}
}

class PostControllerCustomRouteBindingOverridden extends Controller {
	public function show(PostCustomRouteBindingOverridden $post){
		return $post->slug();
	}
}

class BaseModel extends Model {
	use HasHashSlug;
}

class PostExtendingBase extends BaseModel {
	protected $table = "posts";
}